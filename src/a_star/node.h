﻿#pragma once
#include <map>
#include <vector>
#include <iostream>

class Vertex
{
public:
	std::vector<double> lm_state; //left_manipulator_state
	std::vector<double> rm_state; //right_manipulator_state
	std::map<double, double> obj_state;
};

class VertexData
{
public:
	VertexData(
		Vertex ID_,
		std::vector<double> cost_g_,
		std::vector<double> cost_f_);

	Vertex ID;
	std::vector<double> cost_g;
	std::vector<double> cost_f;
	// node, edge cost [my -> neighbor(succ)]
	std::map<Vertex, std::vector<double>> neighbor_ID;
	Vertex camefrom_ID;
};

struct my_compare_struct
{
	bool operator()(VertexData *const l, VertexData *const r)
	{
		if (l->cost_f == r->cost_f)
			return l->cost_g < r->cost_g;
		else
			return l->cost_f < r->cost_f;
	}
};

bool operator<(const Vertex &, const Vertex &);
bool operator>(const Vertex &, const Vertex &);
bool operator==(const Vertex &, const Vertex &);
bool operator!=(const Vertex &, const Vertex &);
void print_vertex(Vertex current_vertex);