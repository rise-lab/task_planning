//ctrl + shift + i
#include <set>
#include <queue>
#include <iostream>
#include <algorithm>
#include "node.h"
#include "calculator.h"
#include "find_map.h"
#include "ros/ros.h"
#include "task_planner/task_msg.h"

Vertex n_vertex;
Vertex s_vertex;
Vertex g_vertex;
Vertex pop_vertex;
task_planner::task_msg msg;
std::map<int, Vertex> path_reverse;
std::vector<std::string> n_vertex_msg;
std::vector<std::string> p_vertex_msg;
std::vector<double> map_path;
std::vector<double> lm_workspace;
std::vector<double> rm_workspace;
std::vector<double>::iterator iter_w;
std::vector<std::vector<double>> pos;
std::vector<std::vector<double>> goal;
std::map<double, std::vector<double>> map_cons;

std::map<Vertex, VertexData *> *M;
std::set<Vertex> closelist;
std::set<Vertex>::iterator iter_c;
std::set<VertexData *, my_compare_struct> openlist;
std::set<VertexData *, my_compare_struct>::iterator iter_o;
std::vector<double> tentative_gScore;
std::vector<double> infinity_pair = {number::infinity, number::infinity};

std::vector<double> heuristic(Vertex a, Vertex b)
{
    if (a.obj_state.size() != b.obj_state.size())
        std::cout << "heuristic function error" << std::endl;
    //key1 : number of gripper operations
    //key2 : distance between vertex_a and vertex_b
    double key1 = 0;
    double key2 = 0;
    std::vector<double> movable_obj;
    std::vector<double> o_lm_w = lm_workspace;
    std::vector<double> o_rm_w = rm_workspace;
    std::vector<double>::iterator workspace_iter;
    std::set<double> lrm_w;
    bool a_l;
    bool b_l;
    bool a_r;
    bool b_r;
    std::set<double> obj_a_l;
    std::set<double> obj_b_l;
    std::set<double> obj_a_r;
    std::set<double> obj_b_r;

    for (const auto lw : lm_workspace)
        for (const auto rw : rm_workspace)
            if (lw == rw)
                lrm_w.insert(lw);
    for (const auto m_w : lrm_w)
    {
        workspace_iter = find(o_lm_w.begin(), o_lm_w.end(), m_w);
        if (workspace_iter != o_lm_w.end()) // Is exist
            o_lm_w.erase(workspace_iter);
        workspace_iter = find(o_rm_w.begin(), o_rm_w.end(), m_w);
        if (workspace_iter != o_rm_w.end()) // Is exist
            o_rm_w.erase(workspace_iter);
    }

    for (unsigned int i = 0; i < a.obj_state.size(); i++)
        if (a.obj_state[i] != b.obj_state[i]) // i -> object to move
            movable_obj.push_back(i);

    for (const auto i : movable_obj)
    {
        a_l = false;
        b_l = false;
        a_r = false;
        b_r = false;
        workspace_iter = find(o_lm_w.begin(), o_lm_w.end(), a.obj_state[i]);
        if (workspace_iter != o_lm_w.end()) // Is exist
            a_l = true;
        workspace_iter = find(o_lm_w.begin(), o_lm_w.end(), b.obj_state[i]);
        if (workspace_iter != o_lm_w.end()) // Is exist
            b_l = true;
        workspace_iter = find(o_rm_w.begin(), o_rm_w.end(), a.obj_state[i]);
        if (workspace_iter != o_rm_w.end()) // Is exist
            a_r = true;
        workspace_iter = find(o_rm_w.begin(), o_rm_w.end(), b.obj_state[i]);
        if (workspace_iter != o_rm_w.end()) // Is exist
            b_r = true;
        if (a_r == false && b_r == true) // left manipulator , obj are both side
        {
            if (a.lm_state[0] == 0 && b.rm_state[0] == 0)
                key1 += 4;
            else if (a.lm_state[0] == 0 && b.rm_state[1] == i)
                key1 += 3;
            else if (a.lm_state[0] == 0 && b.rm_state[1] != i)
                key1 += 4; //5
            else if (a.lm_state[1] == i && b.rm_state[0] == 0)
                key1 += 3;
            else if (a.lm_state[1] == i && b.rm_state[1] == i)
                key1 += 2;
            else if (a.lm_state[1] == i && b.rm_state[1] != i)
                key1 += 4;
            else if (a.lm_state[1] != i && b.rm_state[0] == 0)
                key1 += 4; //5
            else if (a.lm_state[1] != i && b.rm_state[1] == i)
                key1 += 4;
            else if (a.lm_state[1] != i && b.rm_state[1] != i)
                key1 += 4; //6
            else
            {
                printf("error in heuristic1..");
            }
            obj_a_l.insert(i);
            obj_b_r.insert(i);
        }
        else if (a_r == false && b_r == false) // left manipulator , obj are one side
        {
            if (a.lm_state[0] == 0 && b.lm_state[0] == 0)
                key1 += 2;
            else if (a.lm_state[0] == 0 && b.lm_state[1] == i)
                key1 += 1;
            else if (a.lm_state[0] == 0 && b.lm_state[1] != i)
                key1 += 2; //3
            else if (a.lm_state[1] == i && b.lm_state[0] == 0)
                key1 += 1;
            else if (a.lm_state[1] == i && b.lm_state[1] == i)
                key1 += 0; //0
            else if (a.lm_state[1] == i && b.lm_state[1] != i)
                key1 += 2;
            else if (a.lm_state[1] != i && b.lm_state[0] == 0)
                key1 += 2; //3
            else if (a.lm_state[1] != i && b.lm_state[1] == i)
                key1 += 2;
            else if (a.lm_state[1] != i && b.lm_state[1] != i)
                key1 += 2; //4
            else
            {
                printf("error in heuristic2..");
            }
            obj_a_l.insert(i);
            obj_b_l.insert(i);
        }
        else if (a_l == false && b_l == true) // right manipulator , obj are both side
        {
            if (a.rm_state[0] == 0 && b.lm_state[0] == 0)
                key1 += 4;
            else if (a.rm_state[0] == 0 && b.lm_state[1] == i)
                key1 += 3;
            else if (a.rm_state[0] == 0 && b.lm_state[1] != i)
                key1 += 4; //5
            else if (a.rm_state[1] == i && b.lm_state[0] == 0)
                key1 += 3;
            else if (a.rm_state[1] == i && b.lm_state[1] == i)
                key1 += 2;
            else if (a.rm_state[1] == i && b.lm_state[1] != i)
                key1 += 4;
            else if (a.rm_state[1] != i && b.lm_state[0] == 0)
                key1 += 4; //5
            else if (a.rm_state[1] != i && b.lm_state[1] == i)
                key1 += 4;
            else if (a.rm_state[1] != i && b.lm_state[1] != i)
                key1 += 4; //6
            else
            {
                printf("error in heuristic3..");
            }
            obj_a_r.insert(i);
            obj_b_l.insert(i);
        }
        else if (a_l == false && b_l == false) // right manipulator , obj are one side
        {
            if (a.rm_state[0] == 0 && b.rm_state[0] == 0)
                key1 += 2;
            else if (a.rm_state[0] == 0 && b.rm_state[1] == i)
                key1 += 1;
            else if (a.rm_state[0] == 0 && b.rm_state[1] != i)
                key1 += 2; //3
            else if (a.rm_state[1] == i && b.rm_state[0] == 0)
                key1 += 1;
            else if (a.rm_state[1] == i && b.rm_state[1] == i)
                key1 += 0; //0
            else if (a.rm_state[1] == i && b.rm_state[1] != i)
                key1 += 2;
            else if (a.rm_state[1] != i && b.rm_state[0] == 0)
                key1 += 2; //3
            else if (a.rm_state[1] != i && b.rm_state[1] == i)
                key1 += 2;
            else if (a.rm_state[1] != i && b.rm_state[1] != i)
                key1 += 2; //4
            else
            {
                printf("error in heuristic2..");
            }
            obj_a_r.insert(i);
            obj_b_r.insert(i);
        }
        key2 += calc_dist(pos[a.obj_state[i]], pos[b.obj_state[i]]);
    }
    if (movable_obj.size() == 0)
    {
        if (a.lm_state[1] != b.lm_state[1])
        {
            if (a.lm_state[0] != b.lm_state[0])
                key1 += 1;
            else if (a.lm_state[0] == b.lm_state[0])
                key1 += 2;
        }
        if (a.rm_state[1] != b.rm_state[1])
        {
            if (a.rm_state[0] != b.rm_state[0])
                key1 += 1;
            else if (a.rm_state[0] == b.rm_state[0])
                key1 += 2;
        }
    }

    double dist_min = number::infinity;
    for (const auto a_r : obj_a_r)
        if (dist_min > calc_dist(pos[a.rm_state[2]], pos[a.obj_state[a_r]]))
            dist_min = calc_dist(pos[a.rm_state[2]], pos[a.obj_state[a_r]]);
    if (dist_min != number::infinity)
        key2 += dist_min;
    dist_min = number::infinity;
    for (const auto a_l : obj_a_l)
        if (dist_min > calc_dist(pos[a.lm_state[2]], pos[a.obj_state[a_l]]))
            dist_min = calc_dist(pos[a.lm_state[2]], pos[a.obj_state[a_l]]);
    if (dist_min != number::infinity)
        key2 += dist_min;
    dist_min = number::infinity;
    for (const auto b_r : obj_b_r)
        if (dist_min > calc_dist(pos[b.rm_state[2]], pos[b.obj_state[b_r]]))
            dist_min = calc_dist(pos[b.rm_state[2]], pos[b.obj_state[b_r]]);
    if (dist_min != number::infinity)
        key2 += dist_min;
    dist_min = number::infinity;
    for (const auto b_l : obj_b_l)
        if (dist_min > calc_dist(pos[b.lm_state[2]], pos[b.obj_state[b_l]]))
            dist_min = calc_dist(pos[b.lm_state[2]], pos[b.obj_state[b_l]]);
    if (dist_min != number::infinity)
        key2 += dist_min;

    // if (a.lm_state[2] != b.lm_state[2])
    //     key2 += calc_dist(pos[a.lm_state[2]], pos[b.lm_state[2]]);
    // if (a.rm_state[2] != b.rm_state[2])
    //     key2 += calc_dist(pos[a.rm_state[2]], pos[b.rm_state[2]]);
    // std::vector<double> result = {0, 0};
    std::vector<double> result = {key1, key2};
    return result;
}

void input_value()
{
    // s_start is.. { 0,-1, 1} { 0,-1, 5} {{0, 0},{1, 1},{2, 5},{3, 7},{4, 9}} 
    // s_goal is..  { 1, 0, 4} { 0,-1, 6} {{0, 4},{1, 5},{2, 6},{3, 7},{4, 9}}

    // g_vertex.lm_state = {1, 2, 5};
    // g_vertex.rm_state = {0, -1, 1};
    // g_vertex.obj_state = make_map({{0, 0},{1, 1},{2, 5},{3, 7},{4, 9}});
    // s_vertex.lm_state = {0, -1, 6};
    // s_vertex.rm_state = {1, 0, 4};
    // s_vertex.obj_state = make_map({{0, 4},{1, 5},{2, 6},{3, 7},{4, 9}});
    // goal = {{0, 4}};

    g_vertex.lm_state = {0, -1, 4};
    g_vertex.rm_state = {0, -1, 6};
    g_vertex.obj_state = make_map({{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}});
    s_vertex.lm_state = {1, 0, 4};
    s_vertex.rm_state = {0, -1, 6};
    s_vertex.obj_state = make_map({{0, 4}, {1, 6}, {2, 8}, {3, 7}, {4, 9}});
    // goal = {{0, 4}};
    /*
    g_vertex.obj_state = s_vertex.obj_state;
    for (const auto value : goal)
        g_vertex.obj_state[value[0]] = value[1];
    */
}

void pos_generator() //kist algorithm
{
    pos = {{0.86, 0.27}, {0.76, 0.18}, {0.66, 0.30}, {0.56, 0.42}, {0.56, 0.18}, {0.58, -0.15}, {0.60, -0.37}, {0.60, -0.59}, {0.75, -0.37}, {0.75, -0.59}};
    lm_workspace = {0, 1, 2, 3, 4, 5};
    rm_workspace = {5, 6, 7, 8, 9};
}

void map_constraint() //kist algorithm
{
    map_cons.insert(std::make_pair<double, std::vector<double>>(4, {2}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(3, {2}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(2, {1}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(1, {0}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(6, {8}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(7, {9}));
    map_path = {4, 3, 2, 1, 0};
}

void find_goal_obj()
{
    g_vertex.obj_state = s_vertex.obj_state;
    for (const auto value : goal)
        g_vertex.obj_state[value[0]] = value[1];

    // Find the object to move
    std::map<int, std::pair<double, double>> move_obj;
    int num = 0;
    for (const auto i : lm_workspace)
        for (const auto j : rm_workspace)
            if (i == j)
                for (const auto s_obj : s_vertex.obj_state)
                    if (i == s_obj.second)
                        move_obj.insert(std::make_pair(num++, std::make_pair(s_obj.first, s_obj.second)));

    for (const auto i : map_path)
        for (const auto s_obj : s_vertex.obj_state)
            if (s_obj.second == i)
                move_obj.insert(std::make_pair(num++, std::make_pair(s_obj.first, s_obj.second)));
    for (const auto g_p : goal)
        for (const auto m_o : move_obj)
            if (g_p[0] == m_o.second.first)
                move_obj.erase(m_o.first);

    // Find the empty pos
    double que_pos;
    std::queue<double> que;
    std::set<double> empty_pos;
    std::set<double>::iterator iter_ep;
    std::set<double> impossible_pos;
    std::vector<double> infinity_pair = {number::infinity, number::infinity};
    for (const auto i : lm_workspace)
        empty_pos.insert(i);
    for (const auto i : rm_workspace)
        empty_pos.insert(i);

    for (const auto i : map_path)
        impossible_pos.insert(i);
    for (const auto s_obj : s_vertex.obj_state)
    {
        que.empty();
        impossible_pos.insert(s_obj.second);
        if (map_cons.find(s_obj.second) != map_cons.end())
            for (const auto value : map_cons[s_obj.second])
                que.push(value);
        while (que.size() != 0)
        {
            que_pos = que.front();
            impossible_pos.insert(que_pos);
            que.pop();
            if (map_cons.find(que_pos) != map_cons.end())
                for (const auto value : map_cons[que_pos])
                    que.push(value);
        }
    }
    for (const auto im_pos : impossible_pos)
    {
        iter_ep = empty_pos.find(im_pos);
        if (iter_ep != empty_pos.end()) //Is exist
        {
            empty_pos.erase(im_pos);
        }
    }
    bool is_exist = true;
    for (const auto m_o : move_obj)
    {
        for (const auto m_p : map_path)
            if (m_o.second.second == m_p)
                is_exist = false;
        if (is_exist)
            empty_pos.insert(m_o.second.second);
        is_exist = true;
    }

    // Find the goal object state
    std::pair<double, double> f_pos;
    double distance;
    if (empty_pos.size() >= move_obj.size())
    {
        for (const auto m_o : move_obj)
        {
            f_pos = std::make_pair(0, -1);
            for (const auto e_p : empty_pos)
            {
                distance = calc_dist(pos[m_o.second.second], pos[e_p]);
                if (f_pos.first < distance)
                {
                    if (f_pos.second != -1)
                        empty_pos.insert(f_pos.second);
                    f_pos = std::make_pair(distance, e_p);
                    iter_ep = empty_pos.find(f_pos.second);
                    if (iter_ep != empty_pos.end()) //Is exist
                        empty_pos.erase(f_pos.second);
                }
            }
            g_vertex.obj_state[m_o.second.first] = f_pos.second;
        }
    }
    else //추가 작성 요망...
    {
        std::cout << "impossible.." << std::endl;
    }
}

void get_path(Vertex pop_vertex)
{
    // int i = 1000;
    while (pop_vertex != s_vertex)
    {

        // path_reverse[i] = pop_vertex;
        // i--;
        print_vertex(pop_vertex);
        pop_vertex = (*M)[pop_vertex]->camefrom_ID;
    }
    // path_reverse[i] = pop_vertex;
    print_vertex(pop_vertex);
}

void initial_map()
{
    M = new std::map<Vertex, VertexData *>();
    (*M)[s_vertex] = new VertexData(s_vertex, infinity_pair, infinity_pair);
    (*M)[g_vertex] = new VertexData(g_vertex, infinity_pair, infinity_pair);
}

void find_neighbor_vertex(Vertex pop_vertex)
{
    gripper_action(pop_vertex, M);
    manipulator_action(pop_vertex, lm_workspace, rm_workspace, M, map_cons, pos);
}

std::string v_to_string(Vertex current_vertex)
{
    std::string result;
    std::stringstream ss;

    ss << "{";
    for (unsigned int i = 0; i < current_vertex.lm_state.size(); i++)
    {
        ss << current_vertex.lm_state[i];
        if (i != (current_vertex.lm_state.size() - 1))
            ss << ",";
    }
    ss << "}, {";
    for (unsigned int i = 0; i < current_vertex.rm_state.size(); i++)
    {
        ss << current_vertex.rm_state[i];
        if (i != (current_vertex.rm_state.size() - 1))
            ss << ",";
    }
    ss << "}, {";
    for (const auto i : current_vertex.obj_state)
        ss << "{" << i.first << "," << i.second << "}";
    ss << "}";
    result = ss.str();

    return result;
}

void initialize()
{
    pos_generator();
    map_constraint();
    //find_goal_obj();
    initial_map();
    openlist.clear();
    (*M)[s_vertex]->cost_g = {0, 0};
    (*M)[s_vertex]->cost_f = sum_vector((*M)[s_vertex]->cost_g, heuristic(s_vertex, g_vertex));
    openlist.insert((*M)[s_vertex]);
    std::cout << "Complete Initiailize.." << std::endl;
}

void make_msg(Vertex pop_vertex)
{
    n_vertex_msg.clear();
    p_vertex_msg.clear();
    p_vertex_msg.push_back(v_to_string(pop_vertex));
    for (const auto i : (*M)[pop_vertex]->neighbor_ID)
        n_vertex_msg.push_back(v_to_string(i.first));
    msg.neighbor_vertex = n_vertex_msg;
    msg.pop_vertex = p_vertex_msg;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "task_planning_node");
    ros::NodeHandle nh;
    ros::Publisher task_pub =
        nh.advertise<task_planner::task_msg>("/task_planning_topic", 100);
    ros::Rate rate(10);

    input_value();
    initialize();

    int i = 0;
    std::cout << "==============" << std::endl;
    printf("s_start is.. ");
    print_vertex(s_vertex);
    printf("s_goal is..  ");
    print_vertex(g_vertex);
    std::cout << "==============" << std::endl;
    while (openlist.size() != 0)
    {
        i++;
        auto front = openlist.begin();
        pop_vertex = (*front)->ID;
        // print_vertex(pop_vertex);
        if (pop_vertex == g_vertex)
        {
            std::cout << "Pop vertex number is " << i << std::endl;
            std::cout << "solve the task plan.." << std::endl;
            get_path(pop_vertex);
            // for (const auto i : path_reverse)
            // {
            //     print_vertex(i.second);
            // }

            return 0;
        }
        openlist.erase(front);
        closelist.insert(pop_vertex);

        find_neighbor_vertex(pop_vertex);
        //neighbor.first : neighbor ID, neighbor.second : edge cost
        for (const auto neighbor : (*M)[pop_vertex]->neighbor_ID)
        {
            n_vertex = neighbor.first;
            iter_c = closelist.find(n_vertex);
            tentative_gScore = sum_vector((*M)[pop_vertex]->cost_g, neighbor.second);

            if (iter_c != closelist.end()) //Is exist
                continue;

            if (tentative_gScore < (*M)[n_vertex]->cost_g)
            {
                (*M)[n_vertex]->camefrom_ID = pop_vertex;
                (*M)[n_vertex]->cost_g = tentative_gScore;
                (*M)[n_vertex]->cost_f = sum_vector((*M)[n_vertex]->cost_g, heuristic(n_vertex, g_vertex));
                iter_o = openlist.find((*M)[n_vertex]);
                if (iter_o != openlist.end())
                    openlist.erase(iter_o);
                openlist.insert((*M)[n_vertex]);
            }
        }
        make_msg(pop_vertex);
        task_pub.publish(msg);
    }
    std::cout << "have no solution.." << std::endl;

    return 0;
}

// 주석 : ctrl + k + c , 해제 : ctrl + k + u