#include "find_map.h"

void make_edge_vertex(Vertex current_ID, Vertex neighbor_ID,
                      std::vector<double> edge_cost, std::map<Vertex, VertexData *> *M)
{
    std::vector<double> infinity_pair = {number::infinity, number::infinity};
    (*M)[current_ID]->neighbor_ID[neighbor_ID] = edge_cost;
    if ((*M).find(neighbor_ID) == (*M).end()) // Is not exist
        (*M)[neighbor_ID] = new VertexData(neighbor_ID, infinity_pair, infinity_pair);
}

std::vector<double> find_movable_pos(Vertex current_ID, std::vector<double> lm_workspace,
                                     std::vector<double> rm_workspace, std::map<double, std::vector<double>> map_cons, int dirc_m)
{
    std::vector<double> result;
    std::queue<double> que;
    double que_pos;

    // Find the left manipulator's movable place
    if (dirc_m == 0)
    {
        result = lm_workspace;
        result.erase(std::remove(result.begin(), result.end(), current_ID.rm_state[2]), result.end());
        result.erase(std::remove(result.begin(), result.end(), current_ID.lm_state[2]), result.end());
        for (const auto c_o : current_ID.obj_state)
        {
            if (current_ID.lm_state[1] == c_o.first && current_ID.lm_state[0] == 1)
                continue;
            que.empty();
            if (current_ID.lm_state[0] == 1 && current_ID.lm_state[1] != -1) // gripper was closed
                result.erase(std::remove(result.begin(), result.end(), c_o.second), result.end());
            if (map_cons.find(c_o.second) != map_cons.end())
                for (const auto value : map_cons[c_o.second])
                    que.push(value);
            while (que.size() != 0)
            {
                que_pos = que.front();
                result.erase(std::remove(result.begin(), result.end(), que_pos), result.end());
                que.pop();
                if (map_cons.find(que_pos) != map_cons.end())
                    for (const auto value : map_cons[que_pos])
                        que.push(value);
            }
        }
        return result;
    }
    // Find the right manipulator's movable place
    else
    {
        result = rm_workspace;
        result.erase(std::remove(result.begin(), result.end(), current_ID.lm_state[2]), result.end());
        result.erase(std::remove(result.begin(), result.end(), current_ID.rm_state[2]), result.end());
        for (const auto c_o : current_ID.obj_state)
        {
            if (current_ID.rm_state[1] == c_o.first && current_ID.rm_state[0] == 1)
                continue;
            que.empty();
            if (current_ID.rm_state[0] == 1 && current_ID.rm_state[1] != -1) // gripper was closed
                result.erase(std::remove(result.begin(), result.end(), c_o.second), result.end());
            if (map_cons.find(c_o.second) != map_cons.end())
                for (const auto value : map_cons[c_o.second])
                    que.push(value);
            while (que.size() != 0)
            {
                que_pos = que.front();
                result.erase(std::remove(result.begin(), result.end(), que_pos), result.end());
                que.pop();
                if (map_cons.find(que_pos) != map_cons.end())
                    for (const auto value : map_cons[que_pos])
                        que.push(value);
            }
        }
        return result;
    }
}

void manipulator_action(Vertex current_ID, std::vector<double> lm_workspace,
                        std::vector<double> rm_workspace, std::map<Vertex, VertexData *> *M,
                        std::map<double, std::vector<double>> map_cons, std::vector<std::vector<double>> pos)
{
    Vertex neighbor_ID;
    std::vector<double> desired_pos;
    std::vector<double> edge_cost;
    // Move the left manipulator to desired position
    // left manipulator dirc : 0, right manipulator dirc : 1
    desired_pos = find_movable_pos(current_ID, lm_workspace, rm_workspace, map_cons, 0);
    for (auto const d_pos : desired_pos)
    {
        neighbor_ID = current_ID;
        edge_cost = {0, 0};
        edge_cost[1] = calc_dist(pos[d_pos], pos[current_ID.lm_state[2]]);
        if (current_ID.lm_state[0] == 1 && current_ID.lm_state[1] != -1) // gripper was closed
        {
            neighbor_ID.lm_state[2] = d_pos;
            neighbor_ID.obj_state[current_ID.lm_state[1]] = d_pos;
            make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
        }
        else // gripper was opened
        {
            neighbor_ID.lm_state[2] = d_pos;
            make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
        }
    }

    // Move the right manipulator to desired position
    desired_pos = find_movable_pos(current_ID, lm_workspace, rm_workspace, map_cons, 1);
    for (const auto d_pos : desired_pos)
    {
        neighbor_ID = current_ID;
        edge_cost = {0, 0};
        edge_cost[1] = calc_dist(pos[d_pos], pos[current_ID.rm_state[2]]);
        if (current_ID.rm_state[0] == 1 && current_ID.rm_state[1] != -1) // gripper was closed
        {
            neighbor_ID.rm_state[2] = d_pos;
            neighbor_ID.obj_state[current_ID.rm_state[1]] = d_pos;
            make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
        }
        else // gripper was opened
        {
            neighbor_ID.rm_state[2] = d_pos;
            make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
        }
    }
}

void gripper_action(Vertex current_ID, std::map<Vertex, VertexData *> *M)
{
    Vertex neighbor_ID;
    std::vector<double> edge_cost = {1, 0};
    if (current_ID.lm_state[0] == 0 && current_ID.lm_state[1] == -1) // The left gripper was opened
    {
        neighbor_ID = current_ID;
        for (const auto obj : current_ID.obj_state)
        {
            if (current_ID.lm_state[2] == obj.second) // The manipulator was located in the object
            {
                neighbor_ID.lm_state[0] = 1;
                neighbor_ID.lm_state[1] = obj.first;
                make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
            }
            else
            {
                // The manipulator was located empty space
            }
        }
    }
    else if (current_ID.lm_state[0] == 1 && current_ID.lm_state[1] != -1) // The left gripper was closed
    {
        neighbor_ID = current_ID;
        neighbor_ID.lm_state[0] = 0;
        neighbor_ID.lm_state[1] = -1;
        make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
    }

    if (current_ID.rm_state[0] == 0 && current_ID.rm_state[1] == -1) // The right gripper was opened
    {
        neighbor_ID = current_ID;
        for (const auto obj : current_ID.obj_state)
        {
            if (current_ID.rm_state[2] == obj.second) // The manipulator was located in the object
            {
                neighbor_ID.rm_state[0] = 1;
                neighbor_ID.rm_state[1] = obj.first;
                make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
            }
            else
            {
                // The manipulator was located empty space
            }
        }
    }
    else if (current_ID.rm_state[0] == 1 && current_ID.rm_state[1] != -1) // The right gripper was closed
    {
        neighbor_ID = current_ID;
        neighbor_ID.rm_state[0] = 0;
        neighbor_ID.rm_state[1] = -1;
        make_edge_vertex(current_ID, neighbor_ID, edge_cost, M);
    }
}
