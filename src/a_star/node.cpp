﻿#include "node.h"

VertexData::VertexData(
	Vertex ID_,
	std::vector<double> cost_g_,
	std::vector<double> cost_f_)
	: ID(ID_), cost_g(cost_g_), cost_f(cost_f_)
{
}

bool operator<(const Vertex &left, const Vertex &right)
{
	if (left.lm_state < right.lm_state)
		return 1;
	else if (left.lm_state == right.lm_state && left.rm_state < right.rm_state)
		return 1;
	else if (left.lm_state == right.lm_state && left.rm_state == right.rm_state && left.obj_state < right.obj_state)
		return 1;
	return 0;
}

bool operator>(const Vertex &left, const Vertex &right)
{
	if (left.lm_state > right.lm_state)
		return 1;
	else if (left.lm_state == right.lm_state && left.rm_state > right.rm_state)
		return 1;
	else if (left.lm_state == right.lm_state && left.rm_state == right.rm_state && left.obj_state > right.obj_state)
		return 1;
	return 0;
}

bool operator==(const Vertex &left, const Vertex &right)
{
	if (left.lm_state == right.lm_state && left.rm_state == right.rm_state && left.obj_state == right.obj_state)
		return 1;
	return 0;
}
bool operator!=(const Vertex &left, const Vertex &right)
{
	if (left.lm_state != right.lm_state || left.rm_state != right.rm_state || left.obj_state != right.obj_state)
		return 1;
	return 0;
}

void print_vertex(Vertex current_vertex)
{
	std::cout << "{";
	for (const auto i : current_vertex.lm_state)
	{
		if (i < 0)
			std::cout << i << ",";
		else
			std::cout << " " << i << ",";
	}
	std::cout << "\b} {";
	for (const auto i : current_vertex.rm_state)
	{
		if (i < 0)
			std::cout << i << ",";
		else
			std::cout << " " << i << ",";
	}
	std::cout << "\b} {";
	for (const auto i : current_vertex.obj_state)
		std::cout << "{" << i.first << ", " << i.second << "},";
	std::cout << "\b} " << std::endl;
}