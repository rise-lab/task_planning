#pragma once
#include <set>
#include <queue>
#include <iostream>
#include <algorithm>
#include "node.h"
#include "calculator.h"

void write_vertex_pos(std::map<double, std::set<Vertex>> *pos_vertices, Vertex current_vertex);

void gripper_action(Vertex current_ID, std::map<Vertex, VertexData *> *M, std::map<double, std::set<Vertex>> *pos_vertices);

void manipulator_action(Vertex current_ID, std::vector<double> lm_workspace,
                        std::vector<double> rm_workspace, std::map<Vertex, VertexData *> *M,
                        std::map<double, std::vector<double>> map_cons, std::vector<std::vector<double>> pos,
                        std::map<double, std::set<Vertex>> *pos_vertices);