﻿#pragma once
#include <map>
#include <vector>
#include <iostream>

class Vertex
{
public:
	std::vector<double> lm_state; //left_manipulator_state
	std::vector<double> rm_state; //right_manipulator_state
	std::map<double, double> obj_state;
};

class VertexData
{
public:
	VertexData(
		Vertex ID_,
		std::vector<double> cost_g_,
		std::vector<double> cost_rhs_);

	Vertex ID;
	std::vector<double> cost_g;
	std::vector<double> cost_rhs;
	std::pair<std::vector<double>, std::vector<double>> key;

	// node, edge cost [my <- neighbor(succ), my -> neighbor(pred)]
	std::map<Vertex, std::vector<double>> succ;
	std::map<Vertex, std::vector<double>> pred;
};

struct my_compare_struct
{
	bool operator()(VertexData *const l, VertexData *const r)
	{
		if (l->key == r->key)
			return l->cost_rhs < r->cost_rhs;
		else
			return l->key < r->key;
	}
};

bool operator<(const Vertex &, const Vertex &);
bool operator>(const Vertex &, const Vertex &);
bool operator==(const Vertex &, const Vertex &);
bool operator!=(const Vertex &, const Vertex &);

void print_vertex(Vertex current_vertex);
void print_key(std::map<Vertex, VertexData *> *M, Vertex current_vertex);
void print_vector(std::vector<double> vector);