//ctrl + shift + i
// 주석 : ctrl + k + c , 해제 : ctrl + k + u
#include <set>
#include <queue>
#include <iostream>
#include <algorithm>
#include "node.h"
#include "find_map.h"
#include "calculator.h"

Vertex s_start;
Vertex s_goal;
Vertex s_pop;
Vertex s_last;

std::vector<double> succ_rhs;
std::vector<double> succ_value;
bool edge_cost = false;
std::vector<double> k_m;
std::set<Vertex> change_vertices;
std::vector<double> map_path;
std::vector<double> lm_workspace;
std::vector<double> rm_workspace;
std::vector<double>::iterator iter_w;
std::vector<std::vector<double>> pos;
std::vector<std::vector<double>> goal;
std::map<double, double> defalut_obj_map;
std::map<double, std::set<Vertex>> *pos_vertices;
std::map<double, std::vector<double>> map_cons;

std::map<Vertex, VertexData *> *M;
std::map<Vertex, VertexData *> M_;
std::set<VertexData *, my_compare_struct> U;
std::set<VertexData *, my_compare_struct> U_;
std::set<VertexData *, my_compare_struct>::iterator iter_u;
std::vector<double> infinity_pair = {number::infinity, number::infinity};

std::vector<double> heuristic(Vertex a, Vertex b)
{
    if (a.obj_state.size() != b.obj_state.size())
        std::cout << "heuristic function error" << std::endl;
    //key1 : number of gripper operations
    //key2 : distance between vertex_a and vertex_b
    double key1 = 0;
    double key2 = 0;
    std::vector<double> movable_obj;
    std::vector<double> o_lm_w = lm_workspace;
    std::vector<double> o_rm_w = rm_workspace;
    std::vector<double>::iterator workspace_iter;
    std::set<double> lrm_w;
    bool a_l;
    bool b_l;
    bool a_r;
    bool b_r;
    std::set<double> obj_a_l;
    std::set<double> obj_b_l;
    std::set<double> obj_a_r;
    std::set<double> obj_b_r;

    for (const auto lw : lm_workspace)
        for (const auto rw : rm_workspace)
            if (lw == rw)
                lrm_w.insert(lw);
    for (const auto m_w : lrm_w)
    {
        workspace_iter = find(o_lm_w.begin(), o_lm_w.end(), m_w);
        if (workspace_iter != o_lm_w.end()) // Is exist
            o_lm_w.erase(workspace_iter);
        workspace_iter = find(o_rm_w.begin(), o_rm_w.end(), m_w);
        if (workspace_iter != o_rm_w.end()) // Is exist
            o_rm_w.erase(workspace_iter);
    }

    for (unsigned int i = 0; i < a.obj_state.size(); i++)
        if (a.obj_state[i] != b.obj_state[i]) // i -> object to move
            movable_obj.push_back(i);

    for (const auto i : movable_obj)
    {
        a_l = false;
        b_l = false;
        a_r = false;
        b_r = false;
        workspace_iter = find(o_lm_w.begin(), o_lm_w.end(), a.obj_state[i]);
        if (workspace_iter != o_lm_w.end()) // Is exist
            a_l = true;
        workspace_iter = find(o_lm_w.begin(), o_lm_w.end(), b.obj_state[i]);
        if (workspace_iter != o_lm_w.end()) // Is exist
            b_l = true;
        workspace_iter = find(o_rm_w.begin(), o_rm_w.end(), a.obj_state[i]);
        if (workspace_iter != o_rm_w.end()) // Is exist
            a_r = true;
        workspace_iter = find(o_rm_w.begin(), o_rm_w.end(), b.obj_state[i]);
        if (workspace_iter != o_rm_w.end()) // Is exist
            b_r = true;
        if (a_r == false && b_r == true) // left manipulator , obj are both side
        {
            if (a.lm_state[0] == 0 && b.rm_state[0] == 0)
                key1 += 4;
            else if (a.lm_state[0] == 0 && b.rm_state[1] == i)
                key1 += 3;
            else if (a.lm_state[0] == 0 && b.rm_state[1] != i)
                key1 += 4; //5
            else if (a.lm_state[1] == i && b.rm_state[0] == 0)
                key1 += 3;
            else if (a.lm_state[1] == i && b.rm_state[1] == i)
                key1 += 2;
            else if (a.lm_state[1] == i && b.rm_state[1] != i)
                key1 += 4;
            else if (a.lm_state[1] != i && b.rm_state[0] == 0)
                key1 += 4; //5
            else if (a.lm_state[1] != i && b.rm_state[1] == i)
                key1 += 4;
            else if (a.lm_state[1] != i && b.rm_state[1] != i)
                key1 += 4; //6
            else
            {
                printf("error in heuristic1..");
            }
            obj_a_l.insert(i);
            obj_b_r.insert(i);
        }
        else if (a_r == false && b_r == false) // left manipulator , obj are one side
        {
            if (a.lm_state[0] == 0 && b.lm_state[0] == 0)
                key1 += 2;
            else if (a.lm_state[0] == 0 && b.lm_state[1] == i)
                key1 += 1;
            else if (a.lm_state[0] == 0 && b.lm_state[1] != i)
                key1 += 2; //3
            else if (a.lm_state[1] == i && b.lm_state[0] == 0)
                key1 += 1;
            else if (a.lm_state[1] == i && b.lm_state[1] == i)
                key1 += 0; //0
            else if (a.lm_state[1] == i && b.lm_state[1] != i)
                key1 += 2;
            else if (a.lm_state[1] != i && b.lm_state[0] == 0)
                key1 += 2; //3
            else if (a.lm_state[1] != i && b.lm_state[1] == i)
                key1 += 2;
            else if (a.lm_state[1] != i && b.lm_state[1] != i)
                key1 += 2; //4
            else
            {
                printf("error in heuristic2..");
            }
            obj_a_l.insert(i);
            obj_b_l.insert(i);
        }
        else if (a_l == false && b_l == true) // right manipulator , obj are both side
        {
            if (a.rm_state[0] == 0 && b.lm_state[0] == 0)
                key1 += 4;
            else if (a.rm_state[0] == 0 && b.lm_state[1] == i)
                key1 += 3;
            else if (a.rm_state[0] == 0 && b.lm_state[1] != i)
                key1 += 4; //5
            else if (a.rm_state[1] == i && b.lm_state[0] == 0)
                key1 += 3;
            else if (a.rm_state[1] == i && b.lm_state[1] == i)
                key1 += 2;
            else if (a.rm_state[1] == i && b.lm_state[1] != i)
                key1 += 4;
            else if (a.rm_state[1] != i && b.lm_state[0] == 0)
                key1 += 4; //5
            else if (a.rm_state[1] != i && b.lm_state[1] == i)
                key1 += 4;
            else if (a.rm_state[1] != i && b.lm_state[1] != i)
                key1 += 4; //6
            else
            {
                printf("error in heuristic3..");
            }
            obj_a_r.insert(i);
            obj_b_l.insert(i);
        }
        else if (a_l == false && b_l == false) // right manipulator , obj are one side
        {
            if (a.rm_state[0] == 0 && b.rm_state[0] == 0)
                key1 += 2;
            else if (a.rm_state[0] == 0 && b.rm_state[1] == i)
                key1 += 1;
            else if (a.rm_state[0] == 0 && b.rm_state[1] != i)
                key1 += 2; //3
            else if (a.rm_state[1] == i && b.rm_state[0] == 0)
                key1 += 1;
            else if (a.rm_state[1] == i && b.rm_state[1] == i)
                key1 += 0; //0
            else if (a.rm_state[1] == i && b.rm_state[1] != i)
                key1 += 2;
            else if (a.rm_state[1] != i && b.rm_state[0] == 0)
                key1 += 2; //3
            else if (a.rm_state[1] != i && b.rm_state[1] == i)
                key1 += 2;
            else if (a.rm_state[1] != i && b.rm_state[1] != i)
                key1 += 2; //4
            else
            {
                printf("error in heuristic2..");
            }
            obj_a_r.insert(i);
            obj_b_r.insert(i);
        }
        key2 += calc_dist(pos[a.obj_state[i]], pos[b.obj_state[i]]);
    }
    if (movable_obj.size() == 0)
    {
        if (a.lm_state[1] != b.lm_state[1])
        {
            if (a.lm_state[0] != b.lm_state[0])
                key1 += 1;
            else if (a.lm_state[0] == b.lm_state[0])
                key1 += 2;
        }
        if (a.rm_state[1] != b.rm_state[1])
        {
            if (a.rm_state[0] != b.rm_state[0])
                key1 += 1;
            else if (a.rm_state[0] == b.rm_state[0])
                key1 += 2;
        }
    }

    double dist_min = number::infinity;
    for (const auto a_r : obj_a_r)
        if (dist_min > calc_dist(pos[a.rm_state[2]], pos[a.obj_state[a_r]]))
            dist_min = calc_dist(pos[a.rm_state[2]], pos[a.obj_state[a_r]]);
    if (dist_min != number::infinity)
        key2 += dist_min;
    dist_min = number::infinity;
    for (const auto a_l : obj_a_l)
        if (dist_min > calc_dist(pos[a.lm_state[2]], pos[a.obj_state[a_l]]))
            dist_min = calc_dist(pos[a.lm_state[2]], pos[a.obj_state[a_l]]);
    if (dist_min != number::infinity)
        key2 += dist_min;
    dist_min = number::infinity;
    for (const auto b_r : obj_b_r)
        if (dist_min > calc_dist(pos[b.rm_state[2]], pos[b.obj_state[b_r]]))
            dist_min = calc_dist(pos[b.rm_state[2]], pos[b.obj_state[b_r]]);
    if (dist_min != number::infinity)
        key2 += dist_min;
    dist_min = number::infinity;
    for (const auto b_l : obj_b_l)
        if (dist_min > calc_dist(pos[b.lm_state[2]], pos[b.obj_state[b_l]]))
            dist_min = calc_dist(pos[b.lm_state[2]], pos[b.obj_state[b_l]]);
    if (dist_min != number::infinity)
        key2 += dist_min;

    // if (a.lm_state[2] != b.lm_state[2])
    //     key2 += calc_dist(pos[a.lm_state[2]], pos[b.lm_state[2]]);
    // if (a.rm_state[2] != b.rm_state[2])
    //     key2 += calc_dist(pos[a.rm_state[2]], pos[b.rm_state[2]]);
    // std::vector<double> result = {0, 0};
    std::vector<double> result = {key1, key2};
    return result;
}

std::pair<std::vector<double>, std::vector<double>> calculatekey(Vertex s)
{
    std::pair<std::vector<double>, std::vector<double>> result;
    std::vector<double> key1;
    std::vector<double> key2;

    key1 = sum_vector(std::min((*M)[s]->cost_g, (*M)[s]->cost_rhs),
                      heuristic(s, s_start));
    key2 = std::min((*M)[s]->cost_g, (*M)[s]->cost_rhs);
    result = std::make_pair(sum_vector(key1, k_m), key2);
    return result;
}

void input_value()
{
    // s_start is.. { 0,-1, 1} { 1, 2, 5} {{0, 0},{1, 1},{2, 5},{3, 7},{4, 9},{5, 8}}
    // s_goal is..  { 1, 0, 4} { 0,-1, 6} {{0, 4},{1, 5},{2, 6},{3, 7},{4, 9},{5, 8}}
    /**/
    // s_start.lm_state = {0, -1, 1};
    // s_start.rm_state = {1, 2, 5};
    // s_start.obj_state = make_map({{0, 0}, {1, 1}, {2, 5}, {3, 7}, {4, 9}, {5, 8}});
    // s_goal.lm_state = {1, 0, 4};
    // s_goal.rm_state = {0, -1, 6};
    // s_goal.obj_state = make_map({{0, 4}, {1, 5}, {2, 6}, {3, 7}, {4, 9}, {5, 8}});
    // goal = {{0, 4}};
    

    s_start.lm_state = {0, -1, 4};
    s_start.rm_state = {0, -1, 6};
    s_start.obj_state = make_map({{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}});
    s_goal.lm_state = {1, 0, 4};
    s_goal.rm_state = {0, -1, 6};
    goal = {{0, 4}};

    // s_start is.. { 0,-1, 9} { 1, 2, 0} {{0, 8},{1, 9},{2, 0},{3, 3},{4, 4}}
    // s_goal is..  { 0,-1, 8} { 0,-1, 1} {{0, 8},{1, 9},{2, 10},{3, 7},{4, 6}}

    // s_start.rm_state = {0, -1, 8};
    // s_start.lm_state = {0, -1, 1};
    // s_start.obj_state = make_map({{0, 0}, {1, 1}, {2, 2}, {3, 3}});
    // s_goal.rm_state = {0, -1, 8};
    // s_goal.lm_state = {0, -1, 1};
    // s_goal.obj_state = make_map({{0, 8}, {1, 9}, {2, 10}, {3, 7}});
}

void pos_generator() //kist algorithm
{

    pos = {{0.86, 0.27}, {0.76, 0.18}, {0.66, 0.30}, {0.56, 0.42}, {0.56, 0.18}, {0.58, -0.15}, {0.60, -0.37}, {0.60, -0.59}, {0.75, -0.37}, {0.75, -0.59}};
    lm_workspace = {0, 1, 2, 3, 4, 5};
    rm_workspace = {5, 6, 7, 8, 9};

    // pos = {{0.6, -0.15, 0.89}, {0.6, -0.37, 0.89}, {0.6, -0.59, 0.89}, {0.75, -0.26, 0.89}, {0.75, -0.48, 0.89}, {0.72, 0.37, 1.29}, {0.72, 0.15, 1.29}, {0.72, 0.37, 0.99}, {0.72, 0.15, 0.99}, {0.72, 0.37, 0.69}, {0.72, 0.15, 0.69}};
    // rm_workspace = {0, 6, 7, 8, 9, 10};
    // lm_workspace = {0, 1, 2, 3, 4};
}

void map_constraint() //kist algorithm
{
    map_cons.insert(std::make_pair<double, std::vector<double>>(4, {2}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(3, {2}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(2, {1}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(1, {0}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(6, {8}));
    map_cons.insert(std::make_pair<double, std::vector<double>>(7, {9}));
    map_path = {4, 3, 2, 1, 0};

    // map_cons.insert(std::make_pair<double, std::vector<double>>(0, {3}));
    // map_cons.insert(std::make_pair<double, std::vector<double>>(1, {3}));
    // map_cons.insert(std::make_pair<double, std::vector<double>>(1, {4}));
    // map_cons.insert(std::make_pair<double, std::vector<double>>(2, {4}));
}

void find_goal_obj()
{
    s_goal.obj_state = s_start.obj_state;
    for (const auto value : goal)
        s_goal.obj_state[value[0]] = value[1];

    // Find the object to move
    std::map<int, std::pair<double, double>> move_obj;
    int num = 0;
    for (const auto i : lm_workspace)
        for (const auto j : rm_workspace)
            if (i == j)
                for (const auto s_obj : s_start.obj_state)
                    if (i == s_obj.second)
                        move_obj.insert(std::make_pair(num++, std::make_pair(s_obj.first, s_obj.second)));

    for (const auto i : map_path)
        for (const auto s_obj : s_start.obj_state)
            if (s_obj.second == i)
                move_obj.insert(std::make_pair(num++, std::make_pair(s_obj.first, s_obj.second)));
    for (const auto g_p : goal)
        for (const auto m_o : move_obj)
            if (g_p[0] == m_o.second.first)
                move_obj.erase(m_o.first);

    // Find the empty pos
    double que_pos;
    std::queue<double> que;
    std::set<double> empty_pos;
    std::set<double>::iterator iter_ep;
    std::set<double> impossible_pos;
    std::vector<double> infinity_pair = {number::infinity, number::infinity};
    for (const auto i : lm_workspace)
        empty_pos.insert(i);
    for (const auto i : rm_workspace)
        empty_pos.insert(i);

    for (const auto i : map_path)
        impossible_pos.insert(i);
    for (const auto s_obj : s_start.obj_state)
    {
        que.empty();
        impossible_pos.insert(s_obj.second);
        if (map_cons.find(s_obj.second) != map_cons.end())
            for (const auto value : map_cons[s_obj.second])
                que.push(value);
        while (que.size() != 0)
        {
            que_pos = que.front();
            impossible_pos.insert(que_pos);
            que.pop();
            if (map_cons.find(que_pos) != map_cons.end())
                for (const auto value : map_cons[que_pos])
                    que.push(value);
        }
    }
    for (const auto im_pos : impossible_pos)
    {
        iter_ep = empty_pos.find(im_pos);
        if (iter_ep != empty_pos.end()) //Is exist
        {
            empty_pos.erase(im_pos);
        }
    }
    bool is_exist = true;
    for (const auto m_o : move_obj)
    {
        for (const auto m_p : map_path)
            if (m_o.second.second == m_p)
                is_exist = false;
        if (is_exist)
            empty_pos.insert(m_o.second.second);
        is_exist = true;
    }

    // Find the goal object state
    std::pair<double, double> f_pos;
    double distance;
    if (empty_pos.size() >= move_obj.size())
    {
        for (const auto m_o : move_obj)
        {
            f_pos = std::make_pair(0, -1);
            for (const auto e_p : empty_pos)
            {
                distance = calc_dist(pos[m_o.second.second], pos[e_p]);
                if (f_pos.first < distance)
                {
                    if (f_pos.second != -1)
                        empty_pos.insert(f_pos.second);
                    f_pos = std::make_pair(distance, e_p);
                    iter_ep = empty_pos.find(f_pos.second);
                    if (iter_ep != empty_pos.end()) //Is exist
                        empty_pos.erase(f_pos.second);
                }
            }
            s_goal.obj_state[m_o.second.first] = f_pos.second;
        }
    }
    else //추가 작성 요망...
    {
        std::cout << "impossible.." << std::endl;
    }
}

void initial_map()
{
    M = new std::map<Vertex, VertexData *>();
    pos_vertices = new std::map<double, std::set<Vertex>>();
    (*M)[s_start] = new VertexData(s_start, infinity_pair, infinity_pair);
    (*M)[s_goal] = new VertexData(s_goal, infinity_pair, infinity_pair);
    write_vertex_pos(pos_vertices, s_goal);
    write_vertex_pos(pos_vertices, s_start);
}

void initialize()
{
    pos_generator();
    map_constraint();
    find_goal_obj();
    initial_map();
    s_last = (*M)[s_start]->ID;
    U.clear();
    k_m = {0, 0};
    (*M)[s_goal]->cost_rhs = {0, 0};
    (*M)[s_goal]->key = calculatekey(s_goal);
    U.insert((*M)[s_goal]);
    std::cout << "Complete Initiailize.." << std::endl;
}

void find_neighbor_vertex(Vertex pop_vertex)
{
    (*M)[pop_vertex]->pred.empty();
    gripper_action(pop_vertex, M, pos_vertices);
    manipulator_action(pop_vertex, lm_workspace, rm_workspace,
                       M, map_cons, pos, pos_vertices);
}

void updatevertex(Vertex u)
{
    succ_rhs = infinity_pair;
    if (u != s_goal)
    {
        for (const auto succ_ID : (*M)[u]->succ)
        {
            if ((*M).find(succ_ID.first) == (*M).end()) // Is not exist
            {
                std::cout << "skip.." << std::endl;
                continue;
            }
            succ_value = sum_vector(succ_ID.second, (*M)[succ_ID.first]->cost_g);
            if (succ_rhs >= succ_value)
            {
                succ_rhs = succ_value;
                (*M)[u]->cost_rhs = succ_value;
            }
        }
    }
    iter_u = U.find((*M)[u]);
    if (iter_u != U.end()) //Is exist
        U.erase(iter_u);
    if ((*M)[u]->cost_g != (*M)[u]->cost_rhs)
    {
        (*M)[u]->key = calculatekey(u);
        U.insert((*M)[u]);
    }
}

void computeshortestpath()
{
    std::cout << "start computeshortestpath..." << std::endl;
    std::cout << "==============" << std::endl;
    printf("s_start is.. ");
    print_vertex(s_start);
    printf("s_goal is..  ");
    print_vertex(s_goal);
    std::cout << "==============" << std::endl;
    int pop_number = 0;
    while ((*U.begin())->key < calculatekey(s_start) ||
           (*M)[s_start]->cost_rhs != (*M)[s_start]->cost_g)
    {
        pop_number++;
        auto front = U.begin();
        s_pop = (*front)->ID;
        find_neighbor_vertex(s_pop);
        U.erase(front);

        // if (edge_cost == true)
        // {
        //     // std::cout << "==s_pop===" << std::endl;
        //     // print_vertex(s_pop);
        //     // std::cout << "==pred===" << std::endl;
        //     // for (const auto i : (*M)[s_pop]->pred)
        //     // {
        //     //     if ((*M).find(i.first) != (*M).end())
        //     //         print_vertex(i.first);
        //     // }
        //     // std::cout << "===cost===" << std::endl;
        //     // print_vector((*M)[s_pop]->cost_g);
        //     // print_vector((*M)[s_pop]->cost_rhs);
        //     std::cout << "===key===" << std::endl;
        //     print_vector((*M)[s_pop]->key.first);
        //     print_vector((*M)[s_pop]->key.second);
        //     std::cout << "===key_g===" << std::endl;
        //     print_vector((*M)[s_goal]->key.first);
        //     print_vector((*M)[s_goal]->key.second);
        //     std::cout << "===key_s===" << std::endl;
        //     print_vector((*M)[s_start]->key.first);
        //     print_vector((*M)[s_start]->key.second);
        //     std::cout << "===heuristic===" << std::endl;
        //     print_vector(heuristic(s_goal,s_start));
        //     std::cout << "===k_m===" << std::endl;
        //     print_vector(k_m);
        //     std::cout << "==========" << std::endl;

        //     std::getchar();
        // }

        if ((*M)[s_pop]->key < calculatekey(s_pop))
        {
            (*M)[s_pop]->key = calculatekey(s_pop);
            U.insert((*M)[s_pop]);
        }
        else if ((*M)[s_pop]->cost_g > (*M)[s_pop]->cost_rhs)
        {
            (*M)[s_pop]->cost_g = (*M)[s_pop]->cost_rhs;
            for (const auto pred_ID : (*M)[s_pop]->pred)
                updatevertex(pred_ID.first);
        }
        else
        {
            (*M)[s_pop]->cost_g = infinity_pair;
            updatevertex(s_pop);
            for (const auto pred_ID : (*M)[s_pop]->pred)
                updatevertex(pred_ID.first);
        }
    }
    std::cout << "Pop vertex number is " << pop_number << std::endl;
    std::cout << "end computeshortestpath..." << std::endl;
}

std::set<Vertex> check_pos_vertices(int pos_num)
{
    std::set<Vertex> result;
    std::set<Vertex>::iterator r_iter;
    std::map<double, std::set<Vertex>> copy_pos_vertices;
    Vertex pos_ID;
    copy_pos_vertices = *pos_vertices;
    while (copy_pos_vertices[pos_num].size() != 0)
    {
        pos_ID = *copy_pos_vertices[pos_num].begin();
        copy_pos_vertices[pos_num].erase(pos_ID);
        for (const auto p_ID : (*M)[pos_ID]->pred)
        {
            ((*M)[p_ID.first]->succ).erase(pos_ID);
            result.insert(p_ID.first);
        }
        for (const auto s_ID : (*M)[pos_ID]->succ)
            ((*M)[s_ID.first]->pred).erase(pos_ID);
    }

    copy_pos_vertices = *pos_vertices;
    while (copy_pos_vertices[pos_num].size() != 0)
    {
        pos_ID = *copy_pos_vertices[pos_num].begin();
        copy_pos_vertices[pos_num].erase(pos_ID);
        (*M).erase(pos_ID);
        r_iter = result.find(pos_ID);
        if (r_iter != result.end()) //Is exist
            result.erase(r_iter);
    }

    return result;
}

void change_map()
{
    Vertex new_vertex;
    Vertex new_v_pred;
    Vertex new_v_succ;
    M_ = *M;
    (*M).clear();
    for (const auto i : M_)
    {
        new_vertex = i.first;
        for (const auto d_o_m : defalut_obj_map)
            new_vertex.obj_state[d_o_m.first] = d_o_m.second;
        (*M)[new_vertex] = new VertexData(new_vertex, i.second->cost_g, i.second->cost_rhs);
        (*M)[new_vertex]->key = i.second->key;
        for (const auto i_pred : i.second->pred)
        {
            new_v_pred = i_pred.first;
            for (const auto d_o_m : defalut_obj_map)
                new_v_pred.obj_state[d_o_m.first] = d_o_m.second;
            (*M)[new_vertex]->pred[new_v_pred] = i_pred.second;
        }
        for (const auto i_succ : i.second->succ)
        {
            new_v_succ = i_succ.first;
            for (const auto d_o_m : defalut_obj_map)
                new_v_succ.obj_state[d_o_m.first] = d_o_m.second;
            (*M)[new_vertex]->succ[new_v_succ] = i_succ.second;
        }
    }
}

int main(int argc, char *argv[])
{
    input_value();
    initialize();

    computeshortestpath();

    int j = 0;
    while (s_start != s_goal)
    {
        j++;
        print_vertex(s_start);
        if ((*M)[s_start]->cost_g == infinity_pair)
        {
            std::cout << "have no solution.." << std::endl;
            break;
        }
        if (j == 22) // edge cost change..
        {
            /* ------- */
            pos[9] = {0.75, -0.48};
            rm_workspace = {5, 6, 7, 9};
            map_cons[6] = {9};
            find_goal_obj();
            change_vertices = check_pos_vertices(8);
            (*M)[s_goal] = new VertexData(s_goal, infinity_pair, infinity_pair);
            (*M)[s_goal]->cost_rhs = {0, 0};

            /* ------- */
            edge_cost = true;
        }
        if (edge_cost == true)
        {
            U_ = U;
            U.clear();
            while (U_.size() != 0)
            {
                auto U_front = U_.begin();
                for (const auto d_o_m : defalut_obj_map)
                    (*U_front)->ID.obj_state[d_o_m.first] = d_o_m.second;
                if ((*M).find((*U_front)->ID) != (*M).end()) // Is exist
                    U.insert((*M)[(*U_front)->ID]);
                U_.erase(U_front);
            }
            for (const auto d_o_m : defalut_obj_map)
                s_last.obj_state[d_o_m.first] = d_o_m.second;

            k_m = sum_vector(k_m, heuristic(s_last, s_start));
            s_last = s_start;

            Vertex cv;
            while (change_vertices.size() != 0)
            {
                auto p_cv = change_vertices.begin();
                cv = *p_cv;
                for (const auto d_o_m : defalut_obj_map)
                    cv.obj_state[d_o_m.first] = d_o_m.second;

                updatevertex(cv);
                change_vertices.erase(p_cv);
            }

            /* ------- */
            updatevertex(s_goal);
            /* ------- */
            computeshortestpath();
            edge_cost = false;
            print_vertex(s_start);
        }

        std::vector<double> succ_value;
        std::vector<double> succ_value_inf = infinity_pair;
        std::vector<double> k_m_last;
        for (const auto succ_ID : (*M)[s_start]->succ)
        {
            if ((*M).find(succ_ID.first) == (*M).end()) // Is not exist
            {
                std::cout << "skip.." << std::endl;
                continue;
            }
            succ_value = sum_vector(succ_ID.second, (*M)[succ_ID.first]->cost_g);
            if (succ_value_inf > succ_value)
            {
                succ_value_inf = succ_value;
                s_start = succ_ID.first;
                k_m_last = succ_ID.second;
            }
        }

        // k_m = sum_vector(k_m, k_m_last);
    }
    print_vertex(s_start);
    return 0;
}
/*
1. recognition error  //not good
    (edge cost changed)
    s_start.obj_state[3] = 8;
    s_start.obj_state[4] = 7;
    s_goal.obj_state[3] = 8;
    s_goal.obj_state[4] = 7;
    find_goal_obj();
    (*M)[s_goal] = new VertexData(s_goal, infinity_pair, infinity_pair);
    (*M)[s_goal]->cost_rhs = {0, 0};
    (*M)[s_start] = new VertexData(s_start, infinity_pair, infinity_pair);

2. pose error
    (edge cost changed)
    pos[8] = {0.7, -0.57};
    rm_workspace = {4, 5, 7, 8};
    map_cons.erase(6);
    find_goal_obj();
    change_vertices = check_pos_vertices(6);
    (*M)[s_goal] = new VertexData(s_goal, infinity_pair, infinity_pair);
    (*M)[s_goal]->cost_rhs = {0, 0};

    updatevertex(s_goal);
3. detect new obj 
    (edge cost changed) i = 23
    change_vertices = check_pos_vertices(8);
    defalut_obj_map[5] = 8;
    change_map();
    s_start.obj_state[5] = 8;
    find_goal_obj();
    if ((*M).find(s_goal) == (*M).end()) // Is not exist
        (*M)[s_goal] = new VertexData(s_goal, infinity_pair, infinity_pair);
    (*M)[s_goal]->cost_rhs = {0, 0};

4. Organize_obj
    (edge cost changed) i = 22
    change_vertices = check_pos_vertices(3);
    defalut_obj_map[4] = 3;
    change_map();
    s_start.obj_state[4] = 3;
    s_goal.obj_state = make_map({{0, 9}, {1, 10}, {2, 7}, {3, 8}, {4, 6}});
    if ((*M).find(s_goal) == (*M).end()) // Is not exist
        (*M)[s_goal] = new VertexData(s_goal, infinity_pair, infinity_pair);
    (*M)[s_goal]->cost_rhs = {0, 0};
*/
