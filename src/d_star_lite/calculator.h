#pragma once
#include <map>
#include <math.h>
#include <vector>
#include <limits>

namespace number
{ extern double infinity; }

double calc_dist(std::vector<double> p1, std::vector<double> p2);

std::map<double, double> make_map(std::vector<std::vector<double>> Input_value);

std::vector<double> sum_vector(std::vector<double> v1, std::vector<double> v2);