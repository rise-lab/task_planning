#include "calculator.h"

namespace number
{
double infinity = std::numeric_limits<double>::infinity();
}

double calc_dist(std::vector<double> p1, std::vector<double> p2)
{
    double dist_loot = 0;
    double distance;
    bool p1_bool = (p1[1] > 0);
    bool p2_bool = (p2[1] > 0);
    
    std::vector<double> way_point = {0.55, 0.01};
    // std::vector<double> way_point = {0.5, 0, 0.89};
    if (p1_bool != p2_bool)
    {
        distance =0;
        for (unsigned int i = 0; i < p1.size(); i++)
            distance += pow(p1[i] - way_point[i], 2);
        dist_loot += sqrt(distance);
        distance =0;
        for (unsigned int i = 0; i < p2.size(); i++)
            distance += pow(way_point[i] - p2[i], 2);
        dist_loot += sqrt(distance);
    }
    else
    {
        distance =0;
        for (unsigned int i = 0; i < p1.size(); i++)
            distance += pow(p1[i] - p2[i], 2);
        dist_loot += sqrt(distance);
    }
    return dist_loot;
}

std::map<double, double> make_map(std::vector<std::vector<double>> Input_value)
{
    std::map<double, double> result_map;
    for (const auto value : Input_value)
        result_map.insert(std::make_pair(value[0], value[1]));
    return result_map;
}

std::vector<double> sum_vector(std::vector<double> v1, std::vector<double> v2)
{
    std::vector<double> result = v1;
    for (unsigned int i = 0; i < v1.size(); i++)
    {
        result[i] = v1[i] + v2[i];
    }
    return result;
}