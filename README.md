# Task planning 알고리즘


## 실행 코드

1. git clone

    ```
    git clone https://gitlab.com/rise-lab/task_planning.git
    ```

1. catkin_make 실행

    ```
    cd <workspace>
    catkin_make
    ```

1. source 추가

    ```
    source devel/setup.bash
    ```

1. roscore 실행

    ```
    roscore
    ```

1. 작업 계획을 A* 혹은 D* lite 알고리즘으로 실행

    ```
    rosrun task_planner a_star 
    ```
    ```
    rosrun task_planner a_star 
    ```